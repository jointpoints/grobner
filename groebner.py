'''
	File:   groebner.py
	Author: Andrew Yeliseyev (JointPoints), 2020
'''
from sys import path, platform
from os import system
path.insert(1, './src')
from polynomial import *



def s_polynomial(poly1, poly2):
	'''
	Construct S-polynomial
	===
	
	Constructs S-polynomial for two given polynomials.\n
	**Param** `poly1`: `Polynomial` object, first
	polynomial.\n
	**Param** `poly2`: `Polynomial` object, second
	polynomial.\n
	**Returns** `Polynomial` object, the S-polynomial.
	'''
	# Check if both arguments are polynomials
	if not all([isinstance(_, Polynomial) for _ in (poly1, poly2)]):
		raise ValueError('S-polynomial can only be constructed for polynomials.')
	# Check if polynomials are compatible
	if poly1.ring != poly2.ring:
		raise ValueError('S-polynomial can only be constructed for polynomials with coefficients from the same ring.')
	if poly1.var_names != poly2.var_names:
		raise ValueError('S-polynomial can only be constructed for polynomials with the same variables.')
	# Construct S-polynomial
	return Polynomial(poly1.ring, poly1.var_names, {Monomial.lcm(poly1.lm(), poly2.lm()) / poly1.lm() : poly2.lc()}) * poly1 - Polynomial(poly1.ring, poly1.var_names, {Monomial.lcm(poly1.lm(), poly2.lm()) / poly2.lm() : poly1.lc()}) * poly2





def groebner(*polys):
	'''
	Calculate the reduced Gröbner basis
	===
	
	Calculates the reduced Gröbner basis for a given
	set of polynomials.\n
	**Param** `*polys`: `Polynomial` objects, polynomials
	that generate the ideal.\n
	**Returns** a list of `Polynomial` objects, the reduced
	Gröbner basis for this ideal.
	'''
	# Check if any parameters have been passed
	if polys == ():
		raise ValueError('Gröbner basis can only be calculated for a non-empty set of polynomials.')
	# Check if all arguments are polynomials
	if not all([isinstance(_, Polynomial) for _ in polys]):
		raise ValueError('Gröbner basis can only be constructed for polynomials.')
	# Check if polynomials are compatible
	if any([_.ring != polys[0].ring for _ in polys]):
		raise ValueError('Gröbner basis can only be constructed for polynomials with coefficients from the same ring.')
	if any([_.var_names != polys[0].var_names for _ in polys]):
		raise ValueError('Gröbner basis can only be constructed for polynomials with the same variables.')
	# Construct Gröbner basis
	polys = list(polys)
	# 1. Reduce S-polynomials for all polynomials
	i = 0
	while i < len(polys):
		for j in (_ for _ in range(len(polys)) if _ != i):
			S = s_polynomial(polys[i], polys[j])
			residue = S.reduce(*polys)[-1]
			if residue != Polynomial(polys[i].ring, polys[i].var_names, '0') and residue not in polys:
				polys.append(residue)
				i = 0
				break
		else:
			i += 1
	# 2. Reduce the final set of polynomials
	i = 0
	while i < len(polys):
		if polys[i].deg == 0:
			polys[i] = Polynomial(polys[i].ring, polys[i].var_names, '1')
		if polys[:i] + polys[i+1:] != []:
			reduction = polys[i].reduce(*(polys[:i] + polys[i+1:]))
		else:
			break
		if reduction[-1] == Polynomial(polys[i].ring, polys[i].var_names, '0'):
			del polys[i]
		elif reduction[-1] not in polys:
			polys.append(reduction[-1] if reduction[-1].deg > 0 else Polynomial(polys[i].ring, polys[i].var_names, '1'))
			i = 0
		else:
			i += 1
	return tuple(polys)





if __name__ == '__main__':
	def enter_command(question):
		for line in question:
			print(line)
		print('______')
		return input('Enter command: ')
	
	stage = 'start'
	error = ''
	
	n = None
	F = None
	var_names = None
	polys = None
	
	while stage != 'quit':
		system('cls') if platform == 'win32' else system('clear')
		print('* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')
		print('* REDUCED GRÖBNER BASIS CALCULATION                   v.1.0 *')
		print('*                      Andrew Yeliseyev (JointPoints), 2020 *')
		print('* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')
		print()
		print(error)
		print()
		if stage == 'start':
			n = F = None
			var_names = []
			polys = []
			command = enter_command(('What do you wish to do?', '\t1 - Start a new calculation.', '\t2 - Exit program.'))
			stage = {'1' : 'new', '2' : 'quit'}.get(command, stage)
			error = '' if stage != 'start' else 'ERROR! Unknown command. Try again.'
		elif stage == 'new':
			print('Note! You may type in "abort" at any moment instead of any')
			print('value requested by this program in order to get back to the')
			print('start menu. All your progress and entered data will be lost.')
			print('You may try it now to see how it works.')
			command = enter_command(('Enter 1 to continue.',))
			stage = {'1' : 'new1', 'abort' : 'start'}.get(command, stage)
			error = '' if stage != 'new' else 'ERROR! Unknown command. Try again.'
		elif stage == 'new1':
			print('STEP 1/4. Specify the prime base n for a ring Z/nZ of')
			print('integers modulo n.')
			try:
				n = input('Enter n: ')
				stage = 'start' if n == 'abort' else stage
				n = int(n)
				F = ZZ(n)
				if F.euler != n - 1:
					raise ValueError
			except(Exception):
				error = 'ERROR! Base must be prime.' if n != 'abort' else ''
				continue
			stage = 'new2'
			error = ''
		elif stage == 'new2':
			print('STEP 2/4. Enter names of independent variables you wish to')
			print('use in polynomials in the descending order of their priority.')
			print('Visit bitbucket.org/jointpoints/grobner to see the manual')
			print('on how to name variables properly.')
			print('Enter empty line to finish input of variables.')
			for var_i in range(len(var_names)):
				print(f'Variable #{var_i + 1}: {var_names[var_i]}')
			new_var = input(f'Variable #{len(var_names) + 1}: ')
			stage = {'' : 'new3', 'abort' : 'start'}.get(new_var, stage)
			if stage != 'new2':
				error = ''
				continue
			if new_var not in var_names and re.match('[a-zA-Z_]+[^\^*+\-\s]*$', new_var):
				var_names.append(new_var)
				error = ''
			elif new_var in var_names:
				error = 'ERROR! Names must be unique.'
			else:
				error = 'ERROR! Invalid name.'
		elif stage == 'new3':
			if var_names == []:
				stage = 'new2'
				error = 'ERROR! Specify at least one variable.'
				continue
			print('STEP 3/4. Select monomial ordering.')
			print('Visit bitbucket.org/jointpoints/grobner to learn about these')
			print('orderings.')
			command = enter_command(('\t1 - Lex', '\t2 - Deglex'))
			Polynomial.ordering = {'1' : MonomialOrdering.Lex, '2' : MonomialOrdering.Deglex}.get(command, None)
			stage = {'1' : 'new4', '2' : 'new4', 'abort' : 'start'}.get(command, stage)
			error = '' if stage != 'new3' else 'ERROR! Unknown command. Try again.'
		elif stage == 'new4':
			print('STEP 4/4. Enter polynomials.')
			print('Visit bitbucket.org/jointpoints/grobner to learn how to set')
			print('polynomials properly.')
			print('Enter empty line to finish input.')
			for poly_i in range(len(polys)):
				print(f'Polynomial #{poly_i + 1}: {polys[poly_i]}')
			new_poly =  input(f'Polynomial #{len(polys) + 1}: ')
			stage = {'?' : 'new4help', '' : 'finish', 'abort' : 'start'}.get(new_poly, stage)
			if stage != 'new4':
				error = ''
				continue
			try:
				new_poly = Polynomial(F, var_names, new_poly)
				if new_poly not in polys and new_poly != Polynomial(F, var_names, '0'):
					polys.append(new_poly)
					error = ''
				elif new_poly in polys:
					error = 'ERROR! Polynomials must be unique.'
				else:
					error = 'ERROR! Polynomials must be non-zero.'
			except(ValueError):
				error = 'ERROR! Invalid form of polynomial.'
		elif stage == 'finish':
			if polys == []:
				stage = 'new4'
				error = 'ERROR! Specify at least one polynomial.'
				continue			
			print('PROBLEM.')
			print('Calculare a reduced Gröbner basis for')
			for poly in polys:
				print(f'    {poly}')
			print(f'over Z/{n}Z in {dict({MonomialOrdering.Lex : "Lex", MonomialOrdering.Deglex : "Deglex"}).get(Polynomial.ordering)} ordering.\n')
			print('ANSWER.')
			print(groebner(*polys))
			command = enter_command(('\nEnter 1 to get back to the start menu.',))
			stage = {'1' : 'start', 'abort' : 'start'}.get(command, stage)
			error = '' if stage != 'finish' else 'ERROR! Unknown command. Try again.'
	system('cls') if platform == 'win32' else system('clear')
