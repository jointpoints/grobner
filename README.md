# Gröbner basis

## Contents
* [Introduction](#markdown-header-introduction)
* [1. General information](#markdown-header-1-general-information)
    * [1.1. Some definitions](#markdown-header-11-some-definitions)
    * [1.2. Monomial orderings](#markdown-header-12-monomial-orderings)
    * [1.3. Naming the variables](#markdown-header-13-naming-the-variables)
    * [1.4. Specifying polynomials](#markdown-header-14-specifying-polynomials)
* [2. Download](#markdown-header-2-download)
* [3. Requirements](#markdown-header-3-requirements)
* [4. Stand-alone usage](#markdown-header-4-stand-alone-usage)
* [5. Library usage](#markdown-header-5-library-usage)
* [References](#markdown-header-references)

## Introduction
This repository contains a Python script that calculates a reduced Gröbner basis of multivariate polynomials over finite fields with a prime base. Gröbner basis is a useful tool that has many practical applications, solving the systems of polynomial equations over fields being the most remarkable one. The code contained in this repository can be run as an out-of-the-box stand-alone application or it can be used as a Python library for any further development. More information about Gröbner basis can be found in the literature mentioned in [References](#markdown-header-references). Generally, it is assumed that anyone who intends to use this code as a library or a stand-alone application is common with the abstract algebra, particularly with such its sections as ring theory and field theory. All theoretical clarifications below are insufficient and are either given briefly or affiliated with terms the understanding of which here might differ from the standard one.

## 1. General information

### 1.1. Some definitions
The term "multivariate polynomial" names a polynomial that **may** explicitly depend on more than one independent variable. For example:

* *p*(*x*, *y*) = *x²* + *2xy* + *y²*
* *q*(*u*, *v*, *w*) = *uv* + *u* + *100*
* *r*(*z*) = *z*

One may notice that having *w* in the list of variables of *q* is quite pointless since *q* does not really depend on it in any way, however, nothing stops us from doing that. Furthermore, *r* has the only variable *z* in the list of its dependencies and, yet again, it does not violate any of the rules postulated by the given definition.

**Attention!** It is necessary for the calculation of Gröbner basis to have all polynomials depend on the same list of variables, hence, such notation is rather useful.

Let's also notice that it is possible to write *p* in a number of equivalent ways, namely:

* *p*(*x*, *y*) = *x²* + *2xy* + *y²*
* *p*(*x*, *y*) = *x²* + *xy* + *y²* + *xy*
* *p*(*x*, *y*) = (*x* + *y*)²

A representation of a polynomial is called its "standard form" if it is a sum of monomials that are multiplied by their coefficients. Thus, among all representations of *p* from above only the first and the second ones are the standard forms of *p*.

### 1.2. Monomial orderings
Monomial orderings are necessary to divide mutivariate polynomials by each other unambiguously. All orderings that are available in this program are listed in the table below. In the "Description" column polynomials are assumed to depend on *n* variables *x₁*, *x₂*, ..., *xₙ* which are completely ordered *x₁* > *x₂* > ... > *xₙ* (it is possible to interpret this as "*xᵢ* has higher priority than *xⱼ* iff *i* < *j*"). Monomial orderings extend this ordering > from separate variables *x₁*, *x₂*, ..., *xₙ* onto monomials made up of there variables.

|Ordering name|Description|
|:-----------:|:----------|
| `Lex`       |*x₁ᵈ¹¹x₂ᵈ¹²...xₙᵈ¹ⁿ* > *x₁ᵈ²¹x₂ᵈ²²...xₙᵈ²ⁿ* iff there exists *m* such that for all *i* < *m* *d₁ᵢ* = *d₂ᵢ* and *d₁ₘ* > *d₂ₘ*.|
| `Deglex`    |*x₁ᵈ¹¹x₂ᵈ¹²...xₙᵈ¹ⁿ* > *x₁ᵈ²¹x₂ᵈ²²...xₙᵈ²ⁿ* iff *d₁₁* + *d₁₂* + ... + *d₁ₙ* > *d₂₁* + *d₂₂* + ... + *d₂ₙ* or if *d₁₁* + *d₁₂* + ... + *d₁ₙ* = *d₂₁* + *d₂₂* + ... + *d₂ₙ* and *x₁ᵈ¹¹x₂ᵈ¹²...xₙᵈ¹ⁿ* > *x₁ᵈ²¹x₂ᵈ²²...xₙᵈ²ⁿ* in terms of `Lex`.|

It is easy to see that in a case of *n* = *1* all monomial orderings are equivalent.

### 1.3. Naming the variables
This program lets you choose the name of the variables that you wish to use with your polynomials. However, there are limitations to be considered:

* All names **should** be unique;
* A name **should** start with a letter (`a`, `b`, ..., `z`, `A`, `B`, ..., `Z`) or an underscore `_`;
* A name **should not** contain carets `^`, asterics `*`, pluses `+`, minuses `-` or any whitespace characters.

These rules are restricted (i.e. **should**'s are replaced with **must**'s) when the program is used as a stand-alone application. When the program is used as a library, nothing forces a developer to follow these rules, however, one shall be prepared to face lots of problems with parsing correctness.

### 1.4. Specifying polynomials
This program has an internal parser that is capable of parsing strings that contain multivariate polynomials over finite fields with prime base in one of their standard forms. This parser operates due to certain rules:

* Parser neglects all whitespace characters;
* Parser expects string to have nothing but the only polynomial written in it.

Let "monomial" be defined as a string pattern `variable1[^d1][*variable2[^d2]][*variable3[^d3]]...` where `variable1`, `variable2`, `variable3`, etc. are names of different variables (not in any particular order) and `d1`, `d2`, `d3`, etc. are positive integers that represent the powers of respective variables. For instance, these are valid monomial strings in terms of variables `x`, `y` and `z`:

* `y^2*x`
* `z*x^4*y`
* `z`

Here is one more example:

* `x*y*t^3`

This is an invalid string since there is no variable `t`.

Knowing this, a parsable string containig a polynomial in its standard form can be defined as a pattern `[[-]coeff1*][monomial1] [+|- [coeff2*][monomial2]] [+|- [coeff3*][monomial3]]...` where `coeff1`, `coeff2`, `coeff3`, etc. are coefficients represented by non-negative integers (these integers will be interpreted as respective finite field members automatically) and `monomial1`, `monomial2`, `monomial3`, etc. are monomial strings (not in any particular order). Here are some valid examples of polynomial strings:

* `y^2*x + 8*z*x^4*y - z`
* `-10*x +10*x`
* `36`

After parsing such strings this program automatically reduces all homogeneous terms, sorts all monomials due to the selected monomial ordering and sorts all variables within each monomial according to their priorities. Thus, if we consider a field of **Z₅** and `Lex` monomial ordering with `x` > `y` > `z`, then the strings given above will be interpreted respectively as:

* `3*x⁴*y*z + x*y² + 4*z`
* `0`
* `1`

## 2. Download
To download this program select "Downloads" on the navigation bar on the left of this page (or click [here](https://bitbucket.org/jointpoints/grobner/downloads/)) and press the "Download repository" link. This program does not require any specific installation, just unpack the dowloaded compressed zip-file to any directory you prefer.

## 3. Requirements
This program binds you to have:

* 1 MB of hard drive memory;
* Python interpreter, version 3.7 or higher;
* The following Python libraries:
    * `re`;
    * `sys`;
    * `os`.

Amount of needed RAM depends on the difficulty of tasks you wish to solve with this tool.

## 4. Stand-alone usage
To run the program you can:

* Double click the `groebner.py` file;
* Type `python ./groebner.py` in your console, if the `PATH` variable of your operating system's environment contains a path to `python.exe`.

After the program is launched simply follow its instructions printed on the screen.

## 5. Library usage
This tool can also be used as a library for development of your own programs with the help of its methods. To add all functions and classes of it into your code type

    from groebner import *

All available classes and functions are described in the [documentation](https://bitbucket.org/jointpoints/grobner/raw/f8226bd79f6da550f95a2ae202db64acb56c5b5f/Documentation.pdf).

## References

1. K. Moran "Gröbner bases and their Applications", 2008.
2. T.W. Dubé "The Structure of Polynomial Ideals and Gröbner Bases", 1990.
