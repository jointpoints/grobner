'''
	File:   ring_modulo.py
	Author: Andrew Yeliseyev (JointPoints), 2020
'''
from math import gcd



class RingElement:
	'''
	Element of a ring modulo *n*
	===
	
	`RingElement` object represents a single coset within ZZ(n)
	ring.\n
	**Param** `value`: an integer from *0* to *n-1*, a "label" for a
	given coset.\n
	**Param** `modulo`: the base of the ring -- n.\n
	**Param** `euler`: a value of the Euler function for `modulo`.\n
	**Returns** nothing.
	'''
	
	def __init__(self, value, modulo, euler):
		self.value = value
		self.modulo = modulo
		self.euler = euler
	
	
	# a.is_unit()
	def is_unit(self):
		'''
		Check if an element is invertible
		===
		
		Invertible elements, or units, of ring have the inverse
		elements paired with them such that their product equals
		to 1.\n
		**Returns** `True` is the element is a unit, `False`
		otherwise.
		'''
		return gcd(self.value, self.modulo) == 1
	
	
	# a == b
	def __eq__(self, other):
		'''
		Check if `lvalue` equals to `rvalue`
		'''
		# Check if <other> is also a RingElement
		if not isinstance(other, RingElement):
			raise ValueError('Only ZZ ring element can be compared with another ZZ ring element.')
		# Check equality
		return self.value == other.value and self.modulo == other.modulo
	
	
	# a != b
	def __ne__(self, other):
		'''
		Check if `lvalue` does not equal to `rvalue`
		'''
		return not self == other
	
	
	# a + b
	def __add__(self, other):
		'''
		Add `lvalue` to `rvalue`
		===
		
		If both operands belong to the same ring, they are added
		to each other.
		'''
		# Check if <self> and <other> are from the same ring
		if self.modulo != other.modulo:
			raise ValueError('Operations between elements of different rings are impossible.')
		# Add <self> to <other>
		return RingElement((self.value + other.value) % self.modulo, self.modulo, self.euler)
	
	
	# a - b
	def __sub__(self, other):
		'''
		Subtract `rvalue` from `lvalue`
		===
		
		If both operands belong to the same ring, the second one
		is subtracted from the first one.
		'''
		# Check if <self> and <other> are from the same ring
		if self.modulo != other.modulo:
			raise ValueError('Operations between elements of different rings are impossible.')
		# Subtract <other> from <self>
		return RingElement((self.value - other.value) % self.modulo, self.modulo, self.euler)
	
	
	# a * b
	def __mul__(self, other):
		'''
		Multiply `lvalue` by `rvalue`
		===
		
		If both operands belong to the same ring, they are multiplied
		by each other.
		'''
		# Check if <self> and <other> are from the same ring
		if self.modulo != other.modulo:
			raise ValueError('Operations between elements of different rings are impossible.')
		# Multiply <self> by <other>
		return RingElement((self.value * other.value) % self.modulo, self.modulo, self.euler)
	
	
	# a**k
	def __pow__(self, power: int):
		'''
		Exponentiate `lvalue` to the power of `rvalue`
		===
		'''
		if power >= 0:
			return RingElement((self.value**power) % self.modulo, self.modulo, self.euler)
		elif self.value != 0 and self.is_unit():
			return RingElement((self.value**((self.euler - 1)*-power)) % self.modulo, self.modulo, self.euler)
		else:
			raise ValueError('Negative powers are not allowed for zero divisors.')
	
	
	# a / b
	def __truediv__(self, other):
		'''
		Divide `lvalue` by `rvalue`
		===
		
		If both operands belong to the same ring, the first one
		is divided by the second one.
		'''
		# Check if <self> and <other> are from the same ring
		if self.modulo != other.modulo:
			raise ValueError("Operations between elements of different rings are impossible.")
		# Divide <self> by <other>
		try:
			return RingElement((self.value * (other**-1).value) % self.modulo, self.modulo, self.euler)
		except(ValueError):
			raise ValueError('Division by a zero divisor is not allowed.')
	
	
	# Output representations
	def __str__(self):
		return str(self.value)
	
	def __repr__(self):
		return str(self.value)





class ZZ:
	'''
	Ring of integers modulo *n*
	===
	
	`ZZ` objects represent rings of integers modulo *n*.\n
	**Param** `n`: the modulo of a ring.\n
	**Returns** nothing.
	'''
	
	def __init__(self, n):
		# Check if <n> is acceptable
		if not isinstance(n, int):
			raise ValueError('Modulo must be a natural number greater than or equal to 2.')
		if n < 2:
			raise ValueError('Modulo must be a natural number greater than or equal to 2.')
		self.modulo = n
		self.euler = sum([1 for i in range(1, self.modulo) if gcd(i, self.modulo) == 1])
	
	
	# R == P
	def __eq__(self, other):
		'''
		Check if two rings are the same
		===
		'''
		# Check if <other> is also a ring
		if not isinstance(other, ZZ):
			raise ValueError('ZZ rings can only be compared with ZZ rings.')
		return self.modulo == other.modulo
	
	
	# R != P
	def __ne__(self, other):
		'''
		Check if two rings are different
		===
		'''
		return not self == other
	
	
	# R[k]
	def __getitem__(self, k):
		'''
		Access an element of ring
		===
		'''
		return RingElement(k % self.modulo, self.modulo, self.euler)
	
	
	# k in R
	def __contains__(self, k):
		'''
		Check if certain `RingElement` object is a part of
		this ring
		===
		'''
		if not isinstance(k, RingElement):
			return False
		return k.modulo == self.modulo
