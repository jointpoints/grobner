'''
	File:   monomial.py
	Author: Andrew Yeliseyev (JointPoints), 2020
'''



class Monomial:
	'''
	Single monomial
	===
	
	`Monomial` object represents independent variables in certain
	non-negative integer powers muliplied by one another.\n
	This creates a new monomial with specified powers of variables.\n
	**Param** `powers`: list of respective powers.\n
	**Returns** nothing.\n
	**Raises** `ValueError` if no parameters have been passed.\n
	**Raises** `ValueError` if negative or non-integer powers have been
	passed.
	'''
	
	def __init__(self, *powers):
		if powers == () or powers is None:
			raise ValueError('Monomial must contain at least one variable.')
		if not all([isinstance(_, int) for _ in powers]):
			raise ValueError('All powers must be non-negative integers.')
		if any([_ < 0 for _ in powers]):
			raise ValueError('All powers must be non-negative integers.')
		self.powers = powers
		self.deg = sum(self.powers)
	
	
	# lcm(a, b)
	def lcm(*monomials):
		'''
		Least common multiple
		===
		
		Calculates the least common multiple of given monomials.\n
		**Param** `*monomials`: `Monomial` objects.\n
		**Returns** `Monomial` object, the least common multiple.\n
		**Raises** `ValueError` if no arguments are passed.\n
		**Raises** `ValueError` if not all arguments are `Monomial`
		objects.\n
		**Raises** `ValueError` if monomials depend on a different
		number of variables.
		'''
		# Check if there are arguments
		if monomials == ():
			raise ValueError('At least one Monomial is needed.')
		# Check if all <monomials> are Monomial objects
		if not all([isinstance(_, Monomial) for _ in monomials]):
			raise ValueError('All agruments must be Monomial objects')
		# Check if all <monomials> are compatible
		if not all([len(_.powers) == len(monomials[0].powers) for _ in monomials]):
			raise ValueError('Monomials must depend on the same amount of variables.')
		return Monomial(*(max((_.powers[i] for _ in monomials)) for i in range(len(monomials[0].powers))))
	
	
	# a == b
	def __eq__(self, other):
		'''
		Check if monomials are equal
		===
		
		Equality is defined as equality of all corresponding
		powers.\n
		**Returns** `True` if monomials are equal, `False`
		otherwise.
		'''
		# Check if <other> is a monomial
		if not isinstance(other, Monomial):
			return False
		# Check if <self> and <other> are comparable
		if len(self.powers) != len(other.powers):
			return False
		# Check equality
		return all([self.powers[i] == other.powers[i] for i in range(len(self.powers))])
	
	
	# a % b
	def __mod__(self, other):
		'''
		Check if `lvalue` is divisible by an `rvalue`
		===
		
		Monomial *a* is said to be divisible by a monomial *b* if
		there exists a monomial *c* such that *a* = *c* * *b*.\n
		**Returns** `True` if `lvalue` is divisible by an `rvalue`,
		`False` otherwise.
		'''
		# Check if <other> is a monomial
		if not isinstance(other, Monomial):
			return False
		# Check if <self> and <other> are compatible
		if len(self.powers) != len(other.powers):
			return False
		# Power of each variable in <self> must be greater than
		# or equal to the power of this variable in <other>
		return all([self.powers[i] >= other.powers[i] for i in range(len(self.powers))])
	
	
	# a * b
	def __mul__(self, other):
		'''
		Multiply `lvalue` by `rvalue`
		===
		
		Multiplies two monomials by each other.\n
		**Returns** `Monomial` object, the multiple.\n
		**Raises** `ValueError` if `rvalue` is not a `Monomial`
		object.\n
		**Raises** `ValueError` if monomials depend on a different
		number of variables.
		'''
		# Check if <other> is a monomial
		if not isinstance(other, Monomial):
			raise ValueError('Monomials can only be multiplied by other monomials.')
		# Check if <self> and <other> are compatible
		if len(self.powers) != len(other.powers):
			raise ValueError('Monomials can only be multiplied by monomials with the same number of variables.')
		# Add corresponding powers to each other
		return Monomial(*[self.powers[i] + other.powers[i] for i in range(len(self.powers))])
	
	
	# a / b
	def __truediv__(self, other):
		'''
		Divide `lvalue` by `rvalue`
		===
		
		Divides two monomials.\n
		**Returns** `Monomial` object, the quotient.\n
		**Raises** `ValueError` if `rvalue` is not a `Monomial`
		object.\n
		**Raises** `ValueError` if monomials depend on a different
		number of variables.
		'''
		# Check if <other> is a monomial
		if not isinstance(other, Monomial):
			raise ValueError('Monomials can only be divided by other monomials.')
		# Check if <self> and <other> are compatible
		if not self % other:
			raise ValueError('Monomials can only be divided by monomials with the same number of variables.')
		# Divide <self> by <other>
		return Monomial(*[self.powers[i] - other.powers[i] for i in range(len(self.powers))])
	
	
	# Hashable object
	def __hash__(self):
		return tuple(self.powers).__hash__()
