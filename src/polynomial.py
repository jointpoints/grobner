'''
	File:   polynomial.py
	Author: Andrew Yeliseyev (JointPoints), 2020
'''
from monomial import *
from ring_modulo import *
import re



class MonomialOrdering:
	'''
	Monomial orderings
	===
	
	Contains implemented monomial orderings.
	'''

	def Lex(monomials):
		return sorted(monomials, key=lambda x: x.powers, reverse=True)


	def Deglex(monomials):
		return sorted(monomials, key=lambda x: (x.deg, x.powers), reverse=True)





class Polynomial:
	'''
	Polynomial
	===
	
	`Polynomial` object represents a multivariate polynomial over
	a ring of integers modulo *n*. Note, that before performing operations
	with the objects of this class a certain `MonomialOrdering` must be
	set to a `Polynomial.ordering` variable.\n
	This creates a new polynomial over a specified ring with certain
	variables and terms.\n
	**Param** `ring`: a ring of integers modulo *n* from which coefficients
	are taken.\n
	**Param** `var_names`: an ordered iterable of `str` instances specifying
	the names of independent variables in monomials.\n
	**Param** `arg`: a `dict` instance of `Monomial`-coefficient pairs or an `str`
	containig a parsable algebraic expression of a polynomial in its standard
	form.\n
	**Returns** nothing.\n
	**Raises** `ValueError` if any parameter specified above is set incorrectly.\n
	**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
	'''

	ordering = None

	def __init__(self, ring, var_names, arg=None):
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# <ring> must be a ZZ(n)
		if not isinstance(ring, ZZ):
			raise ValueError('Domain of polynomial (argument 1) must be a ring of integers modulo n. Use ZZ class to construct such objects.')
		self.ring = ring
		# <var_names> must be a list of strs
		if not hasattr(var_names, '__getitem__'):
			raise ValueError('Variables of polynomial (argument 2) must be specified as an ordered iterable of str.')
		if not all([isinstance(var, str) for var in var_names]):
			raise ValueError('Variables of polynomial (argument 2) must be specified as an ordered iterable of str.')
		self.var_names = var_names
		# If polynomial is set explicitly
		if isinstance(arg, dict):
			if arg == {}:
				arg = {Monomial(*(0 for i in range(len(self.var_names)))) : ring[0]}
			if not all([isinstance(key, Monomial) and value in ring for key, value in arg.items()]):
				raise ValueError('Each key of dict (argument 3) must be Monomial and each value of dict (argument 3) must be an element from specified ring (argument 1).')
			if not all([len(key.powers) == len(self.var_names) for key in arg.keys()]):
				raise ValueError('All monomials (argument 3) must be compatible with the given set of variables (argument 2).')
			self.terms = arg
		# If polynomial is set with the algebraic expression
		elif isinstance(arg, str):
			arg = arg.replace(' ', '').replace('\t', '').replace('-', '+-').lstrip('+')
			arg = arg if arg != '' else '0'
			pattern_var_names = '|'.join(sorted(var_names, reverse=True))
			pattern_var = f'({pattern_var_names})(\^[1-9][0-9]*)?'
			pattern_monomial = f'({pattern_var})(\*({pattern_var}))*'
			pattern_coefficient = '-?[0-9]*'
			pattern_term = f'(((({pattern_coefficient})(?<!-)\*)|-)?({pattern_monomial}))|(-?[0-9]+)'
			pattern_polynomial = f'({pattern_term})(\+({pattern_term}))*'
			if re.fullmatch(pattern_polynomial, arg) is None:
				raise ValueError('Invalid form of polynomial.')
			str_terms = arg.split('+')
			terms = {}
			for str_term in str_terms:
				coefficient = re.match(pattern_coefficient, str_term).group(0)
				if coefficient == '':
					coefficient = self.ring[1]
				elif coefficient == '-':
					coefficient = self.ring[-1]
				else:
					coefficient = self.ring[int(coefficient)]
				if coefficient != self.ring[0]:
					str_monomial = re.search(pattern_monomial, str_term)
					if str_monomial != None:
						str_monomial = str_monomial.group(0)
						powers = [0] * len(self.var_names)
						while str_monomial != '':
							cur_var = re.match(pattern_var, str_monomial).group(0).split('^')
							powers[self.var_names.index(cur_var[0])] += 1 if len(cur_var) == 1 else int(cur_var[1])
							str_monomial = str_monomial[re.match(pattern_var, str_monomial).span()[1]+1:]
						monomial = Monomial(*powers)
					else:
						monomial = Monomial(*(0 for _ in range(len(self.var_names))))
					if monomial not in terms.keys():
						terms[monomial] = coefficient
					elif terms[monomial] + coefficient != self.ring[0]:
						terms[monomial] += coefficient
					else:
						del terms[monomial]
			self.terms = terms if terms != {} else {Monomial(*(0 for _ in range(len(self.var_names)))) : self.ring[0]}
		else:
			raise ValueError('Polynomial can be defined with either a dict of Monomial-coefficient pairs or a str object with a parsable algebraic expression of a polynomial in its standard form (argument 3).')
		# Calculate the degree of polynomial
		self.deg = max([monomial.deg for monomial in self.terms.keys()])
	
	
	# p.lm()
	def lm(self):
		'''
		Leading monomial
		===
		
		Finds the leading monomial of this polynomial with respect to the specified
		monomial ordering.\n
		**Returns** `Monomial` object, the leading monomial.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		return Polynomial.ordering(list(self.terms.keys()))[0]
	
	
	# p.lc()
	def lc(self):
		'''
		Leading coefficient
		===
		
		Finds the leading coefficient of this polynomial with respect to the specified
		monomial ordering.\n
		**Returns** `RingElement` object, the leading coefficient.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		return self.terms[Polynomial.ordering(list(self.terms.keys()))[0]]
	
	
	# p.lt()
	def lt(self):
		'''
		Leading term
		===
		
		Finds the leading term of this polynomial with respect to the specified
		monomial ordering.\n
		**Returns** `Polynomial` object, the leading term.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		return Polynomial(self.ring, self.var_names, {self.lm() : self.lc()})
	
	
	# p.reduce(...)
	def reduce(self, *others):
		'''
		Reduce polynomial by set
		===
		
		Reduces polynomial by a given set of other polynomials. Results
		may vary for different monomial orderings.\n
		**Param** `*others`: `Polynomial` objects *a1*, *a2*, ..., *aN*
		to be reduced by.\n
		**Returns** list of `Polynomial` objects *q1*, *q2*, ..., *qN*,
		*r*, such that *self* = *q1* * *a1* + *q2* * *a2* + ... + 
		*qN* * *aN* + *r*.\n
		**Raises** `ValueError` if any of passed parameters is not a
		`Polynomial` object.\n
		**Raises** `ValueError` if any two polynomials from a set
		{*self*, *a1*, *a2*, ..., *aN*} have different rings as a domain
		of their coefficients or have different sets of independent
		variables.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# Check if any parameters have been passed
		if others == ():
			raise ValueError('Polynomial can only be reduced by the non-empty set of polynomials.')
		# Check if <others> are also a polynomial
		if not all([isinstance(_, Polynomial) for _ in others]):
			raise ValueError('Polynomial can only be reduced by the set of polynomials.')
		# Check if all polynomials belong to the same polynomial ring
		if any([self.ring != _.ring for _ in others]):
			raise ValueError('Polynomial can only be reduced by polynomials with the coefficients from the same ring.')
		if any([self.var_names != _.var_names for _ in others]):
			raise ValueError('Polynomial can only be reduced by polynomials with the same independent variables.')
		# Reduce polynomial
		result = [Polynomial(self.ring, self.var_names, '0')] * len(others)
		result.append(self)
		others_i = 0
		while others_i < len(others):
			if result[-1] % others[others_i] and result[-1] != Polynomial(self.ring, self.var_names, '0'):
				new_result = result[-1] / others[others_i]
				result[others_i] += new_result[0]
				result[-1] = new_result[1]
				others_i = 0
				continue
			others_i += 1
		return result
	
	
	# p == q
	def __eq__(self, other):
		'''
		Check if polynomials are equal
		===
		
		Equality is defined as equality of all terms.\n
		**Returns** `True` if polynomials are equal, `False`
		otherwise.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# Check if <other> is also a polynomial
		if not isinstance(other, Polynomial):
			return False
		# Check if both polynomials belong to the same polynomial ring
		if self.ring != other.ring:
			return False
		if self.var_names != other.var_names:
			return False
		# Compare polynomials
		return self.terms == other.terms


	# p + q
	def __add__(self, other):
		'''
		Add `lvalue` to `rvalue`
		===
		
		Sum of two polynomials is calculated.\n
		**Returns** `Polynomial` object, the sum.\n
		**Raises** `ValueError` if `rvalue` is not a `Polynomial` object.\n
		**Raises** `ValueError` if polynomials have different rings as a domain
		of their coefficients or have different sets of independent
		variables.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# Check if <other> is also a polynomial
		if not isinstance(other, Polynomial):
			raise ValueError('Only polynomial can be added to polynomial.')
		# Check if both polynomials belong to the same polynomial ring
		if self.ring != other.ring:
			raise ValueError('Only polynomials with the coefficients from the same ring can be added.')
		if self.var_names != other.var_names:
			raise ValueError('Only polynomials with the same independent variables can be added.')
		# Add polynomials
		new_monomials = self.terms.keys() | other.terms.keys()
		new_terms = {}
		for monomial in new_monomials:
			if self.terms.get(monomial, self.ring[0]) + other.terms.get(monomial, self.ring[0]) != self.ring[0]:
				new_terms[monomial] = self.terms.get(monomial, self.ring[0]) + other.terms.get(monomial, self.ring[0])
		return Polynomial(self.ring, self.var_names, new_terms)
	
	
	# p - q
	def __sub__(self, other):
		'''
		Subtract `rvalue` from `lvalue`
		===
		
		Difference of two polynomials is calculated.\n
		**Returns** `Polynomial` object, the difference.\n
		**Raises** `ValueError` if `rvalue` is not a `Polynomial` object.\n
		**Raises** `ValueError` if polynomials have different rings as a domain
		of their coefficients or have different sets of independent
		variables.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		return self + Polynomial(other.ring, other.var_names, {Monomial(*((0,) * len(other.var_names))) : other.ring[-1]}) * other
	
	
	# p * q
	def __mul__(self, other):
		'''
		Muliply `lvalue` by `rvalue`
		===
		
		Product of two polynomials is calculated.\n
		**Returns** `Polynomial` object, the product.\n
		**Raises** `ValueError` if `rvalue` is not a `Polynomial` object.\n
		**Raises** `ValueError` if polynomials have different rings as a domain
		of their coefficients or have different sets of independent
		variables.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# Check if <other> is also a polynomial
		if not isinstance(other, Polynomial):
			raise ValueError('Only polynomial can be multiplied to polynomial.')
		# Check if both polynomials belong to the same polynomial ring
		if self.ring != other.ring:
			raise ValueError('Only polynomials with the coefficients from the same ring can be multiplied.')
		if self.var_names != other.var_names:
			raise ValueError('Only polynomials with the same independent variables can be multiplied.')
		# Multiply polynomials
		new_terms = {}
		for monomial1 in self.terms.keys():
			for monomial2 in other.terms.keys():
				if monomial1 * monomial2 not in new_terms.keys():
					if self.terms[monomial1] * other.terms[monomial2] != self.ring[0]:
						new_terms[monomial1 * monomial2] = self.terms[monomial1] * other.terms[monomial2]
				elif new_terms[monomial1 * monomial2] + self.terms[monomial1] * other.terms[monomial2] != self.ring[0]:
					new_terms[monomial1 * monomial2] += self.terms[monomial1] * other.terms[monomial2]
				else:
					del new_terms[monomial1 * monomial2]
		return Polynomial(self.ring, self.var_names, new_terms)
	
	
	# p % q
	def __mod__(self, other):
		'''
		Check if `lvalue` is divisible by `rvalue`
		===
		
		Polynomial *p* is said to be divisible by a polynomial *f* if
		there exists a polynomial *q* such that *p* = *q* * *f*.\n
		**Returns** `True` if `lvalue` is divisible by `rvalue`, `False`
		otherwise.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# Check if <other> is also a polynomial
		if not isinstance(other, Polynomial):
			return False
		# Check if both polynomials belong to the same polynomial ring
		if self.ring != other.ring:
			return False
		if self.var_names != other.var_names:
			return False
		# Check divisibility
		return self.lm() % other.lm() and other.lc().is_unit()
	
	
	# p / q
	def __truediv__(self, other):
		'''
		Divide `lvalue` by `rvalue`
		===
		
		Quotient and residue of division are calculated.\n
		**Returns** `Polynomial` objects *q* and *r*, quotient and residue,
		respectively.\n
		**Raises** `ValueError` if `rvalue` is not a `Polynomial` object.\n
		**Raises** `ValueError` if polynomials have different rings as a domain
		of their coefficients or have different sets of independent
		variables.\n
		**Raises** `RuntimeError` if `Polynomial.ordering` is not set.
		'''
		# <ordering> must be set first
		if Polynomial.ordering not in MonomialOrdering.__dict__.values():
			raise RuntimeError('Polynomial.ordering must be set before any actions with Polynomial objects are possible. Use functions from MonomialOrdering to define this value.')
		# Check if <other> is also a polynomial
		if not isinstance(other, Polynomial):
			raise ValueError('Polynomial can only be divided by polynomial.')
		# Check if both polynomials belong to the same polynomial ring
		if self.ring != other.ring:
			raise ValueError('Only polynomials with the coefficients from the same ring can be divided.')
		if self.var_names != other.var_names:
			raise ValueError('Only polynomials with the same independent variables can be divided.')
		# Divide <self> by <other>
		quotient = Polynomial(self.ring, self.var_names, '0')
		residue = self
		while residue % other and residue != Polynomial(self.ring, self.var_names, '0'):
			quotient_monomial = residue.lm() / other.lm()
			quotient_coefficient = residue.lc() / other.lc()
			residue -= Polynomial(self.ring, self.var_names, {quotient_monomial : quotient_coefficient}) * other
			quotient += Polynomial(self.ring, self.var_names, {quotient_monomial : quotient_coefficient})
		return (quotient, residue)


	# Output representations
	def __str__(self):
		# Get the right order of monomials
		order = Polynomial.ordering(list(self.terms.keys()))
		unicode_powers = str.maketrans('0123456789', '⁰¹²³⁴⁵⁶⁷⁸⁹')
		if self.deg > 0:
			return ' + '.join(['*'.join(filter(None,[str(self.terms[order[i]]) if self.terms[order[i]].value > 1 or order[i].deg == 0 else '', *list([self.var_names[j] + str(order[i].powers[j]).translate(unicode_powers) if order[i].powers[j] > 1 else self.var_names[j] if order[i].powers[j] == 1 else '' for j in range(len(self.var_names))])])) for i in range(len(order))])
		else:
			return str(self.terms[order[0]])

	def __repr__(self):
		return self.__str__()
